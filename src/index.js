
import React, { useState } from 'react';
import {
    TouchableOpacity,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import { ModalColor } from './components/Modal';



export default function Index(props) {
    const [modal,setModal]=useState(false)
    const [color,setColor]=useState('black')


    return (
        <View style={{height:'100%',width:'100%',justifyContent:'center'}}>
            <View style={{width:'80%',height:'50%',borderWidth:5,borderColor:'blue',borderRadius:10,alignSelf:'center'}}>
                <View style={{alignSelf:'center',alignItems:'center',height:'20%',justifyContent:'flex-end'}}>
                   <Text  style={{color:color}}  >Prueba Básica</Text> 
                </View>
                <View style={{justifyContent:'space-between',flexDirection:'row',height:'20%', width:'80%',alignSelf:'center'}}>
                    
                    <TouchableOpacity onPress={()=>{setModal(!modal)
                                                    setColor('red')    
                                                } }
                        style={{alignSelf:'center',backgroundColor: 'red',width:'80%',height:'50%',borderRadius:5,width:'40%',justifyContent:'center',marginTop:'10%'}}>
                </TouchableOpacity> 
                <TouchableOpacity onPress={()=>{setModal(!modal)
                                                 setColor('blue')     
                                            } }
                        style={{alignSelf:'center',backgroundColor: 'blue',width:'80%',height:'50%',borderRadius:5,width:'40%',justifyContent:'center',marginTop:'10%'}}>
                </TouchableOpacity> 
                </View> 
             <ModalColor isVisible={modal} color={color} closeModal={(flag)=>setModal(flag)}/>   
            </View>
        
        </View>
      );
}