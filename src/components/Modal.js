import React from 'react';
import {
  View,Text
} from 'react-native';
import Modal from "react-native-modal";
export const ModalColor =(props) =>{
  const {isVisible,color,closeModal} = props;
  console.log("llego al modal")
  return(
        <Modal
                children={1}
                isVisible={isVisible}
                onRequestClose={() => {
                    closeModal(false)
                }}
                onBackButtonPress={() => {
                    closeModal(false)
                }}
                onBackdropPress={() => {
                    closeModal(false)
                }}
            >
            <View style={{
                width:'95%',
                height:'35%',
                justifyContent:'center',
                backgroundColor:'white',
                borderRadius:30,
                borderWidth:2,
                borderColor:'blue'}}>
                <View> 
                    <View style={{justifyContent:'center',width:'80%',alignSelf:'center'}}>
                        <Text 
                            style={{
                                alignSelf:'center',
                                fontFamily:'sans-serif-light',
                                fontWeight:'bold',
                                color:'black',
                                fontSize:19}}> señor usuario el color de la pantalla
                                ha cambiado a 
                            </Text>
                            <Text 
                            style={{
                                alignSelf:'center',
                                fontFamily:'sans-serif-light',
                                fontWeight:'bold',
                                justifyContent:'center',
                                color:color,
                                fontSize:19}}>{color}
                            </Text>     
                    </View>
                    
                </View>
                
            </View>
      
        </Modal>
  )
}